CC = gcc
CFLAGS = -O2
VPATH = src

OBJTESTES = testes.o stringset.o resposta.o pedido.o \
				map_hash_nficheiros.o hash_nficheiros.o avl.o

OBJSERVIDOR = servidor.o utils.o stringset.o resposta.o pedido.o \
				map_hash_nficheiros.o hash_nficheiros.o avl.o

OBJCLIENTE = cliente.o utils.o stringset.o resposta.o pedido.o avl.o \
				map_hash_nficheiros.o hash_nficheiros.o


all: sobucli sobuserv
	rm -f *.o

sobucli: $(OBJCLIENTE)
	$(CC) $(CFLAGS) -o sobucli $(OBJCLIENTE)

sobuserv: $(OBJSERVIDOR)
	$(CC) $(CFLAGS) -o sobuserv $(OBJSERVIDOR)

testes: $(OBJTESTES)
	$(CC) $(CFLAGS) -o testes $(OBJTESTES)

servidor.o: pedido.h resposta.h map_hash_nficheiros.h stringset.h utils.h
cliente.o: pedido.h resposta.h stringset.h utils.h
utils.o: utils.h map_hash_nficheiros.h stringset.h
testes.o: map_hash_nficheiros.h stringset.h
stringset.o: avl.h stringset.h
resposta.o: resposta.h
pedido.o: pedido.h
map_hash_nficheiros.o: map_hash_nficheiros.h hash_nficheiros.h avl.h
hash_nficheiros.o: hash_nficheiros.h avl.h
avl.o: avl.h

.PHONY: samehash
samehash: sobuserv sobucli
	cp -f exemplos/lorem_ipsum.txt .
	mv -f lorem_ipsum.txt same_hash1.txt
	cp -f exemplos/lorem_ipsum.txt .
	mv -f lorem_ipsum.txt same_hash2.txt
	./sobucli backup same_hash1.txt same_hash2.txt

.PHONY: samename
samename: sobuserv sobucli
	cp -f exemplos/lorem_ipsum2.txt .
	mv -f lorem_ipsum2.txt same_name.txt
	./sobucli backup same_name.txt
	echo "aaaaaa" >> same_name.txt
	./sobucli backup same_name.txt

.PHONY: testrun
testrun: sobuserv sobucli
	cp -f exemplos/lorem_ipsum.txt .
	mv -f lorem_ipsum.txt same_hash1.txt
	cp -f exemplos/lorem_ipsum.txt .
	mv -f lorem_ipsum.txt same_hash2.txt
	./sobucli backup same_hash1.txt same_hash2.txt
	cp -f exemplos/lorem_ipsum2.txt .
	mv -f lorem_ipsum2.txt same_name.txt
	./sobucli backup same_name.txt
	echo "aaaaaa" >> same_name.txt
	./sobucli backup same_name.txt


.PHONY: install
install:
	mkdir Backup
	mkdir Backup/data
	mkdir Backup/metadata


.PHONY: clean
clean:
	rm -f testes sobucli sobuserv *.o src/*.gch  Backup/resposta_[0-9]* Backup/ficheiros_[0-9]* Backup/pipe_pedidos Backup/data/* Backup/metadata/* *.txt