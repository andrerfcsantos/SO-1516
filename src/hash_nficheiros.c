#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hash_nficheiros.h"
#include "avl.h"

HASH_NFICHEIROS hnf_inicializaVazio(){
    return hnf_inicializa(NULL, 0);
}

HASH_NFICHEIROS hnf_inicializaComHash(char *hash){
    return hnf_inicializa(hash, 0);
}

HASH_NFICHEIROS hnf_inicializa(char *hash, int n){
    HASH_NFICHEIROS res = (HASH_NFICHEIROS) malloc(sizeof(struct s_hash_nficheiros));
    res->hash = hash ? strdup(hash) : NULL;
    res->nFicheiros = n;
    return res;
}

int hnf_getNFicheiros(HASH_NFICHEIROS p){
    return p->nFicheiros;
}

char *hnf_getHash(HASH_NFICHEIROS p){
    return p->hash;
}

char *hnf_toString(HASH_NFICHEIROS p){
    char *res;
    int res_asprintf;
    res_asprintf = asprintf(&res, "HASH_NFICHEIROS{ hash = %s, n_ficheiros = %d }", 
                                    p->hash, p->nFicheiros);
    if(res_asprintf==-1) res = NULL;
    return res;
}


void hnf_free(HASH_NFICHEIROS p){
    if(p!=NULL){
        free(p->hash);
    }
    free(p);
}

