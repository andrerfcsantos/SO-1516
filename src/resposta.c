#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include "resposta.h"

AP_RESPOSTA inicializaResposta(TIPO_RESPOSTA p_tr){
    AP_RESPOSTA res = (AP_RESPOSTA) malloc(sizeof(RESPOSTA));
    res->tr = p_tr;
    return res;
}

char *respostaToString(AP_RESPOSTA r){
    char *res;
    int res_asprintf;
    res_asprintf = asprintf(&res, "RESPOSTA{tipo_resposta = %d (%s)}", 
                                    r->tr, tipoRespostaToString(r->tr));
    if(res_asprintf==-1) res=NULL;
    return res;
}

void freeResposta(AP_RESPOSTA r){
    free(r);
}

char *tipoRespostaToString(TIPO_RESPOSTA r){
    char *res;
    
    switch(r){
        case OK: res = "OK"; break;
        case NOT_OK: res = "NOT_OK"; break;
        case NO_ACCESS_TO_FILE: res = "NO_ACCESS_TO_FILE"; break;
        case BACKUP_ERROR: res = "BACKUP_ERROR"; break;
        case RESTORE_ERROR: res = "RESTORE_ERROR"; break;
        case DELETE_ERROR: res = "DELETE_ERROR"; break;
        case GC_ERROR: res = "GC_ERROR"; break;
        case SERVER_BUSY: res = "SERVER_BUSY"; break;
        default: res = "Undefined"; break;
    }
    
    return res;
}







