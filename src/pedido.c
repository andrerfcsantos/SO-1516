#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include "pedido.h"


AP_PEDIDO inicializaPedidoVazio(){
    AP_PEDIDO res = (AP_PEDIDO) malloc(sizeof(PEDIDO));
    return res;
}

AP_PEDIDO inicializaPedido(TIPO_PEDIDO p_tp, pid_t p_pid){
    AP_PEDIDO res = (AP_PEDIDO) malloc(sizeof(PEDIDO));
    res->pid = p_pid;
    res->tp = p_tp;
    return res;
}

char* pedidoToString(AP_PEDIDO p){
    char *res;
    int res_asprintf;
    res_asprintf = asprintf(&res, "PEDIDO{tipo_pedido = %d (%s), pid_cliente = %d}",
                                    p->tp, tipoPedidoToString(p->tp), p->pid);
    if(res_asprintf==-1) res = NULL;
    return res;
}

void freePedido(AP_PEDIDO p){
    free(p);    
}

char *tipoPedidoToString(TIPO_PEDIDO tp){
    char *res;
    
    switch(tp){
        case BACKUP:  res = "BACKUP"; break;
        case RESTORE: res = "RESTORE"; break;
        case DELETE: res = "DELETE"; break;
        case GC: res = "GC"; break;
        case BACKUP_DIR: res = "BACKUP_DIR"; break;
        default: res = "Desconhecido"; break;
    }
    
    return res;
}

