#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "map_hash_nficheiros.h"
#include "hash_nficheiros.h"
#include "avl.h"

static int comparaHashAVL(const void *avl_a, const void *avl_b, void *param);
static void freeHashAVL(void *p, void *param);

MAP_HASH_NFICHEIROS mhnf_inicializa(){
    MAP_HASH_NFICHEIROS res = (MAP_HASH_NFICHEIROS) malloc(sizeof(struct s_hash_nficheiros));
    res->arvore = avl_create(comparaHashAVL, NULL, NULL);
    return res;
}

void mhnf_addHash(MAP_HASH_NFICHEIROS m, char *hash){
    HASH_NFICHEIROS procura = hnf_inicializa(hash, 1);
    HASH_NFICHEIROS res_procura = avl_find(m->arvore, procura);
    
    if(res_procura!=NULL){
        //Se já existir nodo, adicionar corresponde a incrementar
        //o número de ficheiros. Fazer free ao nodo "dummy" de procura
        res_procura->nFicheiros++;
        hnf_free(procura);
    }else{
        //Nodo não existe. Necessário criar um novo.
        //Em vez de criar nodo novo, aproveita-se o nodo "dummy" 
        //que se criou para a procura
        avl_insert(m->arvore, procura);
    }
    
}

void mhnf_decrementaHash(MAP_HASH_NFICHEIROS m, char *hash, int deleteIfZero){
    HASH_NFICHEIROS procura = hnf_inicializa(hash, 1);
    HASH_NFICHEIROS res_procura = avl_find(m->arvore, procura);
    
    if(res_procura!=NULL){
        if(hnf_getNFicheiros(res_procura)==1 && deleteIfZero){
            //avl_delete() apenas elimina associação da árvore, não o nodo em si
            //no entanto o nodo é devolvido para que se possa fazer free
            hnf_free(avl_delete(m->arvore, res_procura));
        }else{
            res_procura->nFicheiros--;
        }   
    }
    
    hnf_free(procura);
}

void mhnf_eliminaHash(MAP_HASH_NFICHEIROS m, char *hash){
    if(m!=NULL) 
        hnf_free(avl_delete(m->arvore, hash));
}

int mhnf_getNFicheirosHash(MAP_HASH_NFICHEIROS m, char *hash){
    int res=-1;
    HASH_NFICHEIROS procura = hnf_inicializa(hash, 1);
    HASH_NFICHEIROS res_procura = avl_find(m->arvore, procura);
    
    if(res_procura!=NULL) res = hnf_getNFicheiros(res_procura);
    
    hnf_free(procura);
    return res;
}


void mhnf_free(MAP_HASH_NFICHEIROS m){
    if(m!=NULL) avl_destroy(m->arvore, freeHashAVL);
    free(m);
}

void mhnf_print(MAP_HASH_NFICHEIROS m){
    HASH_NFICHEIROS hnf;
    char *hnf_str;
    TRAVERSER it =  avl_t_aInit(m->arvore);
    
    while((hnf=avl_t_next(it))!=NULL){
        hnf_str = hnf_toString(hnf);
        printf("%s\n", hnf_str);
        free(hnf_str);
    }
    
    avl_t_free(it);
}

static int comparaHashAVL(const void *avl_a, const void *avl_b, void *param){
    HASH_NFICHEIROS a = (HASH_NFICHEIROS) avl_a;
    HASH_NFICHEIROS b = (HASH_NFICHEIROS) avl_b;
    return strcmp(a->hash, b->hash);
}

static void freeHashAVL(void *p, void *param){
    HASH_NFICHEIROS h = (HASH_NFICHEIROS) p;
    hnf_free(h);
}
