#define _GNU_SOURCE
#include <stdio.h> /* asprintf, printf*/
#include <stdlib.h> /* malloc, free */
#include <string.h> /* strdup */
#include <sys/types.h> /* wait, open, mkfifo */
#include <sys/stat.h> /* wait, open, mkfifo */
#include <fcntl.h> /* open */
#include <unistd.h> /* pipe */
#include <errno.h> /* errno */
#include <signal.h> /* signal */
#include "pedido.h"
#include "resposta.h"
#include "map_hash_nficheiros.h"
#include "stringset.h"
#include "utils.h"

int numProcessos=0;
char *pathMetadata;
char *pathData;
char *pathBackup;
char *pathPipePedidos;
char *pathPipeRespostas;
char *pathPipeFicheiros;


int initServidor(void);
int initPathFichPedidos(pid_t pid);
void trataBackupServidor(PEDIDO p);
void trataRestoreServidor(PEDIDO p);
void trataDeleteServidor(PEDIDO p);
void trataGcServidor(PEDIDO p);
char *calculaHash(char *ficheiro);
char *getHashMetadata(char *ficheiro);
int fazBackup(char *ficheiro, char *hash);
int fazRestore(char *hash, char *ficheiro);
int fazDelete(MAP_HASH_NFICHEIROS info, char *ficheiro, char *hash);
int fazGc(STRINGSET info);
int executaRm(char *ficheiro);
int executaCP(char *ficheiro);
int executaGunzip(char *ficheiro);
int executaMv(char *hash, char *ficheiro);
int existeFicheiro(char *ficheiro);
int existeFicheiroMetadata(char *ficheiro);
void trataSIGCHLD(int sgnum);



int main(int argc, char** argv) {
    pid_t pid_filho;
    int fd_pipePedidos, fd_pipeRespostas;
    int lido, escrito, res_initPaths;
    PEDIDO p;
    AP_RESPOSTA r;
    
    if(initServidor()==0){
        fprintf(stderr, "[SERVIDOR - ERRO] Impossivel inicializar servidor. A sair...\n");
        return (EXIT_SUCCESS) ;
    }
    
    signal(SIGCHLD, trataSIGCHLD);
    //Abre pipe para receber pedidos
    fd_pipePedidos = open(pathPipePedidos, O_RDONLY);
    if(fd_pipePedidos==-1){
        fprintf(stderr, "[SERVIDOR - ERRO] Impossivel abrir pipe de pedidos. A sair...\n");
        return (EXIT_SUCCESS);
    }

    while (1) {
        if ((lido = read(fd_pipePedidos, &p, sizeof (PEDIDO))) > 0) {
            res_initPaths = initPathFichPedidos(p.pid);
            
            if (numProcessos < 2 && res_initPaths==1) {
                numProcessos++;
                printf("[SERVIDOR - RECEBIDO] %s\n", pedidoToString(&p));
                pid_filho = fork();

                if (pid_filho == 0) {
                    close(fd_pipePedidos);
                    signal(SIGCHLD, SIG_DFL);
                    switch (p.tp) {
                        case BACKUP: trataBackupServidor(p);
                            break;
                        case RESTORE: trataRestoreServidor(p);
                            break;
                        case DELETE: trataDeleteServidor(p);
                            break;
                        case GC: trataGcServidor(p);
                            break;
                        case BACKUP_DIR: trataBackupServidor(p);
                            break;
                        default: fprintf(stderr, "[SERVIDOR - ERRO] Pedido desconhecido\n");
                            break;
                    }
                    _exit(112);
                }
                
            } else {
                //Abre pipe para escrever respostas
                fd_pipeRespostas = open(pathPipeRespostas, O_WRONLY);
                
                if(fd_pipeRespostas==-1){
                    fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir pipe de resposta para recusar pedido\n");
                }
                
                //Resposta de recusa do pedido
                r = inicializaResposta(SERVER_BUSY);
                escrito = write(fd_pipeRespostas, r, sizeof (RESPOSTA));
                if(escrito != -1){
                    printf("[SERVIDOR - ENVIADO] %s\n", respostaToString(r));
                }else{
                    fprintf(stderr, "[SERVIDOR - ERRO] Erro ao escrever resposta de recusa do pedido\n");
                }
                
                freeResposta(r);
                close(fd_pipeRespostas);
            }
        }
    }


    close(fd_pipePedidos);
    return (EXIT_SUCCESS);
}

int initServidor() {
    int res_mkfifo;
    int res_mkdir;
    int res_asprintf;
    
    pathBackup = strdup("Backup/");
    pathData = strdup("Backup/data/");
    pathMetadata = strdup("Backup/metadata/");
    
    res_mkdir = mkdir(pathBackup, 0775);
    
    if(res_mkdir == -1 && errno != EEXIST){
        fprintf(stderr, "[SERVIDOR - ERRO] Impossivel criar pasta Backup. A sair...");
        return 0;
    }
    
    res_mkdir = mkdir(pathData, 0775);
    
    if(res_mkdir == -1 && errno != EEXIST){
        fprintf(stderr, "[SERVIDOR - ERRO] Impossivel criar pasta data. A sair...");
        return 0;
    }
    
    res_mkdir = mkdir(pathMetadata, 0775);
    
    if(res_mkdir == -1 && errno != EEXIST){
        fprintf(stderr, "[SERVIDOR - ERRO] Impossivel criar pasta metadata. A sair...");
        return 0;
    }
    
    res_asprintf = asprintf(&pathPipePedidos, "%spipe_pedidos", pathBackup);
    
    res_mkfifo = mkfifo(pathPipePedidos, 0666);
    
    if (res_asprintf==-1 || (res_mkfifo == -1 && errno != EEXIST)) {
        fprintf(stderr, "[SERVIDOR - ERRO] Impossivel criar pipe pedidos. A sair...");
        return 0;
    }
    
    return 1;
}

int initPathFichPedidos(pid_t pid){
    int res_asprintf;
    res_asprintf = asprintf(&pathPipeRespostas, "%sresposta_%d",pathBackup, pid);
    if(res_asprintf == -1){
        fprintf(stderr, "[SERVIDOR - ERRO] Impossivel determinar nome de pipe resposta\n");
        return 0;
    }
    res_asprintf = asprintf(&pathPipeFicheiros, "%sficheiros_%d",pathBackup, pid);
    if(res_asprintf == -1){
        fprintf(stderr, "[SERVIDOR - ERRO] Impossivel determinar nome de pipe ficheiro\n");
        return 0;
    }
    return 1;
}

void trataBackupServidor(PEDIDO p) {
    AP_RESPOSTA r;
    int lido, escrito;
    int fd_pipeFicheiros, fd_pipeRespostas;
    char ficheiro[MAX_SIZE_PATH], *hash;

    //Abre pipe para escrever respostas
    fd_pipeRespostas = open(pathPipeRespostas, O_WRONLY);
    if (fd_pipeRespostas == -1) {
        fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir pipe de resposta\n");
        return;
    }

    //Resposta de aceitação do pedido
    r = inicializaResposta(OK);
    escrito = write(fd_pipeRespostas, r, sizeof (RESPOSTA));
    if(escrito == -1){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro ao escrever em pipe de resposta\n");
        return;
    }else{
        printf("[SERVIDOR - ENVIADO] %s\n", respostaToString(r));
    }
    
    //Abre pipe de ficheiros
    fd_pipeFicheiros = open(pathPipeFicheiros, O_RDONLY);
    if(fd_pipeFicheiros==-1){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir em pipe de ficheiros\n");
        return;
    }

    while ((lido = read(fd_pipeFicheiros, ficheiro, MAX_SIZE_PATH)) > 0) {

        printf("[SERVIDOR - RECEBIDO] Ficheiro: %s\n", ficheiro);

        if (existeFicheiro(ficheiro)) {
            hash = calculaHash(ficheiro);
            printf("Hash: %s\n", hash);

            if (fazBackup(ficheiro, hash) != -1) {
                r = inicializaResposta(OK);
            } else {
                r = inicializaResposta(BACKUP_ERROR);
            }

            escrito = write(fd_pipeRespostas, r, sizeof (RESPOSTA));

            if (escrito == -1){
                fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir em pipe de respostas\n");
            }else printf("[SERVIDOR - ENVIADO] %s\n", respostaToString(r));
            
        } else {
            r = inicializaResposta(NO_ACCESS_TO_FILE);
            escrito = write(fd_pipeRespostas, r, sizeof (RESPOSTA));
            
            if (escrito == -1){
                fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir em pipe de respostas\n");
            }else printf("[SERVIDOR - ENVIADO] %s\n", respostaToString(r));
        }


    }

}

void trataRestoreServidor(PEDIDO p) {
    AP_RESPOSTA r;
    int lido, escrito, res_asprintf;
    int fd_pipeFicheiros, fd_pipeRespostas;
    char ficheiro[MAX_SIZE_PATH], *hash, *fichMetadata;

    //Abre pipe para escrever respostas
    fd_pipeRespostas = open(pathPipeRespostas, O_WRONLY);
    if (fd_pipeRespostas == -1) {
        fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir pipe de resposta\n");
        return;
    }

    //Resposta de aceitação do pedido
    r = inicializaResposta(OK);
    escrito = write(fd_pipeRespostas, r, sizeof (RESPOSTA));
    if(escrito == -1){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro ao escrever em pipe de resposta\n");
        return;
    }else{
        printf("[SERVIDOR - ENVIADO] %s\n", respostaToString(r));
    }
    
    //Abre pipe de ficheiros
    fd_pipeFicheiros = open(pathPipeFicheiros, O_RDONLY);
    if(fd_pipeFicheiros==-1){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir em pipe de ficheiros\n");
        return;
    }
    
    
    while ((lido = read(fd_pipeFicheiros, ficheiro, MAX_SIZE_PATH)) > 0) {

        printf("[SERVIDOR - RECEBIDO] Ficheiro: %s\n", ficheiro);
        
        res_asprintf = asprintf(&fichMetadata, "%s%s", pathMetadata, ficheiro);
        
        if(res_asprintf==-1){
            fprintf(stderr, "[SERVIDOR - ERRO] Erro a determinar path ficheiro no metadata\n");
        }
        
        if (existeFicheiro(fichMetadata)) {
            hash = getHashMetadata(ficheiro);
            printf("Hash: %s\n", hash);

            if (fazRestore(hash,ficheiro) != -1 && hash !=NULL) {
                r = inicializaResposta(OK);
            } else {
                r = inicializaResposta(RESTORE_ERROR);
            }

            escrito = write(fd_pipeRespostas, r, sizeof (RESPOSTA));

             if (escrito == -1){
                fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir em pipe de respostas\n");
            }else printf("[SERVIDOR - ENVIADO] %s\n", respostaToString(r));
            
        } else {
            r = inicializaResposta(NO_ACCESS_TO_FILE);
            escrito = write(fd_pipeRespostas, r, sizeof (RESPOSTA));
            
            if (escrito == -1){
                fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir em pipe de respostas\n");
            }else printf("[SERVIDOR - ENVIADO] %s\n", respostaToString(r));
        }


    }


}

void trataDeleteServidor(PEDIDO p) {
    AP_RESPOSTA r;
    int lido, escrito;
    int fd_pipeFicheiros, fd_pipeRespostas;
    char ficheiro[MAX_SIZE_PATH], *hash;
    MAP_HASH_NFICHEIROS metadataInfo = loadMetadata(pathMetadata);
    
    //Abre pipe para escrever respostas
    fd_pipeRespostas = open(pathPipeRespostas, O_WRONLY);
    if (fd_pipeRespostas == -1) {
        fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir pipe de resposta\n");
        return;
    }

    //Resposta de aceitação do pedido
    r = inicializaResposta(OK);
    escrito = write(fd_pipeRespostas, r, sizeof (RESPOSTA));
    if(escrito == -1){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro ao escrever em pipe de resposta\n");
        return;
    }else{
        printf("[SERVIDOR - ENVIADO] %s\n", respostaToString(r));
    }
    
    //Abre pipe de ficheiros
    fd_pipeFicheiros = open(pathPipeFicheiros, O_RDONLY);
    if(fd_pipeFicheiros==-1){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir em pipe de ficheiros\n");
        return;
    }
    
    
    while ((lido = read(fd_pipeFicheiros, ficheiro, MAX_SIZE_PATH)) > 0) {

        printf("[SERVIDOR - RECEBIDO] Ficheiro: %s\n", ficheiro);

        if (existeFicheiroMetadata(ficheiro)) {
            hash = getHashMetadata(ficheiro);
            printf("Hash: %s\n", hash);

            if (fazDelete(metadataInfo, ficheiro, hash) != -1) {
                r = inicializaResposta(OK);
            } else {
                r = inicializaResposta(DELETE_ERROR);
            }

            escrito = write(fd_pipeRespostas, r, sizeof (RESPOSTA));

            if (escrito == -1){
                fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir em pipe de respostas\n");
            }else printf("[SERVIDOR - ENVIADO] %s\n", respostaToString(r));
        } else {
            r = inicializaResposta(NO_ACCESS_TO_FILE);
            escrito = write(fd_pipeRespostas, r, sizeof (RESPOSTA));

            if (escrito == -1){
                fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir em pipe de respostas\n");
            }else printf("[SERVIDOR - ENVIADO] %s\n", respostaToString(r));
        }


    }
    mhnf_free(metadataInfo);
}

void trataGcServidor(PEDIDO p) {
    AP_RESPOSTA r;
    int escrito;
    int fd_pipeRespostas;
    STRINGSET dataInfo = loadHashsMetadata(pathMetadata);

    //Abre pipe para escrever respostas
    fd_pipeRespostas = open(pathPipeRespostas, O_WRONLY);
    if (fd_pipeRespostas == -1) {
        fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir pipe de resposta\n");
        return;
    }

    //Resposta de aceitação do pedido
    r = inicializaResposta(OK);
    escrito = write(fd_pipeRespostas, r, sizeof (RESPOSTA));
    if (escrito == -1) {
        fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir em pipe de respostas\n");
    } else printf("[SERVIDOR - ENVIADO] %s\n", respostaToString(r));


    if (fazGc(dataInfo) != -1) {
        r = inicializaResposta(OK);
    } else {
        r = inicializaResposta(GC_ERROR);
    }

    escrito = write(fd_pipeRespostas, r, sizeof (RESPOSTA));

    if (escrito == -1) {
        fprintf(stderr, "[SERVIDOR - ERRO] Erro ao abrir em pipe de respostas\n");
    } else printf("[SERVIDOR - ENVIADO] %s\n", respostaToString(r));


    strset_free(dataInfo);
}

char *calculaHash(char *ficheiro) {
    int pid_pipes[2];
    int lido, res_exec, res_pipe;
    char *res = (char *) malloc(sizeof (char)*41);

    res_pipe = pipe(pid_pipes);
    if(res_pipe == -1){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro ao criar pipe anonimo\n");
        return "";
    }
    
    pid_t pid_filho = fork();

    if (pid_filho == 0) {
        dup2(pid_pipes[PIPE_ESCRITA], 1);
        close(pid_pipes[PIPE_ESCRITA]);
        close(pid_pipes[PIPE_LEITURA]);

        res_exec = execlp("sha1sum", "sha1sum", ficheiro, NULL);
        if (res_exec == -1) {
            printf("Falha no exec sha1sum %s\n", ficheiro);
            _exit(0);
        }
    } else {
        close(pid_pipes[PIPE_ESCRITA]);
        lido = read(pid_pipes[PIPE_LEITURA], res, 40);
        res[40] = '\0';

        close(pid_pipes[PIPE_LEITURA]);
    }

    return res;
}

char* getHashMetadata(char *ficheiro){
    char *res = (char *) malloc(sizeof(char)*41);
    char *pathFichMetadata;
    int fd_metadata, res_asprintf;
    
    res_asprintf = asprintf(&pathFichMetadata, "%s%s", pathMetadata, ficheiro);
    
    if(res_asprintf==-1){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro determinar path de ficheiro no metadata\n");
        return NULL;
    }
    
    fd_metadata = open(pathFichMetadata, O_RDONLY);
    
    if(fd_metadata == -1){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro determinar abrir ficheiro em metadata\n");
        return NULL;
    }
    
    if(read(fd_metadata, res, 40) <=0){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro ao ler ficheiro em metadata\n");
        return NULL;
    }
    res[40] = '\0';
    
    close(fd_metadata);
    return res;
}

int fazBackup(char *ficheiro, char *hash) {
    pid_t pid_filho, res_wait;
    int res=1;
    int estado, escrito;
    int fd_fichData, fd_fichMetadata;
    int res_exec, res_asprintf;
    char *pathFichData, *pathFichMetadata;

    res_asprintf = asprintf(&pathFichData, "%s%s.gz",pathData, hash);
    
     if(res_asprintf==-1){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro determinar path de ficheiro no metadata\n");
        return -1;
    }
    
    res_asprintf = asprintf(&pathFichMetadata, "%s%s", pathMetadata, basename(ficheiro));

    if (res_asprintf == -1) {
        fprintf(stderr, "[SERVIDOR - ERRO] Erro determinar path de ficheiro no metadata\n");
        return -1;
    }

    fd_fichData = open(pathFichData, O_WRONLY | O_CREAT | O_TRUNC, 0666);
    fd_fichMetadata = open(pathFichMetadata, O_WRONLY | O_CREAT | O_TRUNC, 0666);

    if (fd_fichData == -1 || fd_fichMetadata == -1) {
        close(fd_fichData);
        close(fd_fichMetadata);
        return -1;
    }

    escrito = write(fd_fichMetadata, hash, 41);

    pid_filho = fork();

    if (pid_filho == 0) {
        dup2(fd_fichData, 1);
        close(fd_fichData);
        close(fd_fichMetadata);

        res_exec = execlp("gzip", "gzip", "-c", ficheiro, NULL);

        if(res_exec == -1)printf("Falha no exec gzip %s\n", ficheiro);
        _exit(112);

    } else {
        close(fd_fichData);
        close(fd_fichMetadata);
        res_wait = waitpid(pid_filho, &estado, 0);

        if (res_wait != -1 && WIFEXITED(estado)) {
            if (WEXITSTATUS(estado) == 112) res = -1;
        } else {
            printf("Erro ao esperar por filho ou filho saiu de forma inesperada\n");
        }
    }

    return res;
}

int fazRestore(char *hash, char *ficheiro){
    int res=1, res_cp, res_gz, res_mv, res_asprintf;
    char *pathFicheiroData, *copia;
    
    res_asprintf= asprintf(&pathFicheiroData, "Backup/data/%s.gz", hash);
    
    if(res_asprintf==-1){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro determinar path de ficheiro data\n");
        return -1;
    }
    
    res_asprintf = asprintf(&copia, "%s.gz", hash);
    
    if(res_asprintf==-1){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro determinar ficheiro de copia\n");
        return -1;
    }
    
    
    if(!existeFicheiro(pathFicheiroData)) res=-1;
    
    if(res!=-1){
        res_cp = executaCP(pathFicheiroData);
        if(res_cp==-1) res =-1;
    }
    
    if(res!=-1){
        res_gz = executaGunzip(copia);
        if(res_gz==-1) res =-1;
    }
    
    if(res!=-1){
        res_mv = executaMv(hash, ficheiro);
        if(res_mv==-1) res =-1;
    }
    
    free(pathFicheiroData);
    free(copia);
    return res;
}

int fazDelete(MAP_HASH_NFICHEIROS info, char *ficheiro, char *hash){
    int res=1, res_rm, res_asprintf;
    char *pathFicheiroData, *pathFicheiroMetadata;
    
    res_asprintf = asprintf(&pathFicheiroData, "%s%s.gz", pathData, hash);
    if(res_asprintf==-1){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro determinar path de ficheiro data\n");
        return -1;
    }
    res_asprintf = asprintf(&pathFicheiroMetadata, "%s%s", pathMetadata, ficheiro);
        
    if(res_asprintf==-1){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro determinar path de ficheiro data\n");
        return -1;
    }
    
    
    if(res!=-1){
        res_rm = executaRm(pathFicheiroMetadata);
        if(res_rm==-1) res =-1;
    }
    
    if(res!=-1 && mhnf_getNFicheirosHash(info, hash) <= 1){
        res_rm = executaRm(pathFicheiroData);
        if(res_rm==-1) res =-1;
    }
    
    
    free(pathFicheiroData);
    free(pathFicheiroMetadata);
    return res;
}

int fazGc(STRINGSET info) {
    int res = 1, res_rm, tamanho, i=0;
    char *ficheiroData;
    char **ficheiros;
    STRINGSET set_ficheiros;
   
    set_ficheiros = daFicheirosDir(pathData);
    ficheiros = strset_toArray(set_ficheiros);

    for(i=0;ficheiros[i];i++){
        ficheiroData = strdup(basename(ficheiros[i]));
        tamanho = strlen(ficheiroData);
        //Elimina extensão .gz
        ficheiroData[tamanho - 3] = '\0';
        if (!strset_existestr(info, ficheiroData)) {
            res_rm = executaRm(ficheiros[i]);
            if (res_rm == -1) printf("Erro ao executar rm!");
        }
        free(ficheiroData);
    }

    return res;
}

int executaRm(char *ficheiro) {
    pid_t pid_filho;
    int res_exec, res_wait, res = 1, estado;

    pid_filho = fork();

    if (pid_filho == 0) {
        res_exec = execlp("rm", "rm", "-f", ficheiro, NULL);

        if (res_exec == -1) printf("Falha no exec do rm %s\n", ficheiro);
        _exit(112);
    } else {

        res_wait = waitpid(pid_filho, &estado, 0);
        if (res_wait != -1 && WIFEXITED(estado)) {
            if (WEXITSTATUS(estado) == 112) res = -1;
        } else {
            printf("Erro ao esperar por filho ou filho saiu de forma inesperada\n");
        }
    }

    return res;
}

int executaCP(char *ficheiro){
    pid_t pid_filho;
    int res_exec, res_wait, res=1, estado;
    
    pid_filho = fork();
    
    if(pid_filho ==0){
        res_exec = execlp("cp", "cp", "-f", ficheiro, ".", NULL);
        
        if(res_exec == -1) printf("Falha no exec gzip %s\n", ficheiro);
        _exit(112);
    }else{
        
        res_wait = waitpid(pid_filho, &estado, 0);
        if (res_wait != -1 && WIFEXITED(estado)) {
            if (WEXITSTATUS(estado) == 112) res = -1;
        } else {
            printf("Erro ao esperar por filho ou filho saiu de forma inesperada\n");
        }
    }
    
    return res;
}

int executaGunzip(char *ficheiro){
    pid_t pid_filho;
    int res_exec, res_wait, res=1, estado;
    
    pid_filho = fork();
    
    if(pid_filho ==0){
        res_exec = execlp("gunzip", "gunzip", ficheiro, NULL);
        
        if(res_exec == -1) printf("Falha no exec gunzip %s\n", ficheiro);
        _exit(112);
    }else{
        
        res_wait = waitpid(pid_filho, &estado, 0);
        if (res_wait != -1 && WIFEXITED(estado)) {
            if (WEXITSTATUS(estado) == 112) res = -1;
        } else {
            printf("Erro ao esperar por filho ou filho saiu de forma inesperada\n");
        }
    }
    
    return res;
}

int executaMv(char *hash, char *ficheiro){
    pid_t pid_filho;
    int res_exec, res_wait, res=1, estado;
    
    pid_filho = fork();
    
    if(pid_filho ==0){
        res_exec = execlp("mv", "mv", "-f", hash, ficheiro, NULL);
        
        if(res_exec == -1) printf("Falha no exec mv %s\n", ficheiro);
        _exit(112);
    }else{
        
        res_wait = waitpid(pid_filho, &estado, 0);
        if (res_wait != -1 && WIFEXITED(estado)) {
            if (WEXITSTATUS(estado) == 112) res = -1;
        } else {
            printf("Erro ao esperar por filho ou filho saiu de forma inesperada\n");
        }
    }
    
    return res;
}

int existeFicheiro(char *ficheiro) {
    int res = 0;
    int fd = open(ficheiro, O_RDONLY);
    if (fd > 0) res = 1;
    close(fd);
    return res;
}

int existeFicheiroMetadata(char *ficheiro) {
    int res=0, fd, res_asprintf;
    char *pathCompleto;
    
    res_asprintf = asprintf(&pathCompleto, "%s%s", pathMetadata, ficheiro);
    if(res_asprintf==-1){
        fprintf(stderr, "[SERVIDOR - ERRO] Erro determinar path de ficheiro data\n");
        return 0;
    }
    
    
    fd = open(pathCompleto, O_RDONLY);
    if (fd > 0) res = 1;
    close(fd);
    free(pathCompleto);
    return res;
}

void trataSIGCHLD(int sgnum){
    pid_t pid;
    int estado;
    if(sgnum == SIGCHLD){
        pid = wait(&estado);
        numProcessos--;
        printf("[SERVIDOR] Pedido concluido! Processos actuais: %d (pid: %d | estado:%d)\n", numProcessos, pid, WEXITSTATUS(estado));
    }
}
