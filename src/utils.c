#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "utils.h"
#include "map_hash_nficheiros.h"
#include "stringset.h"

ssize_t leLinha(int fildes, char *buff_original, size_t nbyte){
    int end =0;
    ssize_t lido;
    size_t nbytes_leitura = 1;
    size_t nbytes_lidos_total = 0;
    
    while(nbytes_lidos_total < nbyte && !end) {
        end=0;
        
        lido = read(fildes,buff_original+nbytes_lidos_total,nbytes_leitura);
        
        if(buff_original[nbytes_lidos_total] == '\n' || lido <=0){
            end=1;
            buff_original[nbytes_lidos_total]='\0';
        }
        nbytes_lidos_total += lido;
        
    }
    
    return nbytes_lidos_total;
}

STRINGSET daFicheirosDir(char *directoria){
    int estado, lido, res_pipe;
    char fichLido[MAX_LINHA];
    int fd_pipe[2];
    pid_t pid_filho;
    STRINGSET res = strset_inicializa();
    
    res_pipe = pipe(fd_pipe);
    if(res_pipe==-1) return NULL;
    
    pid_filho = fork();
    
    if(pid_filho==0){
        dup2(fd_pipe[1],1);
        close(fd_pipe[1]);
        close(fd_pipe[0]);
        execlp("find", "find", directoria, "-type", "f", NULL);
        _exit(112);
    }else{
        close(fd_pipe[1]);
        while((lido = leLinha(fd_pipe[0], fichLido ,MAX_LINHA))!=0){
            strset_add(res, fichLido);
        }
        close(fd_pipe[0]);
        waitpid(pid_filho, &estado,0);
    }
    
    return res;
}


MAP_HASH_NFICHEIROS loadMetadata(char *metadataPath) {
    char **ficheiros;
    int fd_ficheiro, i=0, res_read;
    STRINGSET set_ficheiros;
    MAP_HASH_NFICHEIROS map = mhnf_inicializa();
    char metadataInfo[41];

    set_ficheiros = daFicheirosDir(metadataPath);
    ficheiros = strset_toArray(set_ficheiros);

    for (i = 0; ficheiros[i]; i++) {
        fd_ficheiro = open(ficheiros[i], O_RDONLY);
        if (fd_ficheiro != -1) {
            res_read = read(fd_ficheiro, metadataInfo, 40);
            if (res_read != -1) {
                metadataInfo[40] = '\0';
                mhnf_addHash(map, metadataInfo);
            }
        }
        close(fd_ficheiro);
    }

    return map;
}

STRINGSET loadHashsMetadata(char *metadataPath) {
    char **ficheiros;
    int fd_ficheiro, i=0, res_read;
    STRINGSET set_ficheiros;
    STRINGSET res = strset_inicializa();
    char metadataInfo[41];
    
    set_ficheiros = daFicheirosDir(metadataPath);
    ficheiros = strset_toArray(set_ficheiros);
    

    for (i = 0; ficheiros[i]; i++) {
        fd_ficheiro = open(ficheiros[i], O_RDONLY);
        if (fd_ficheiro != -1) {
            res_read = read(fd_ficheiro, metadataInfo, 40);
            if (res_read != -1) {
                metadataInfo[40] = '\0';
                strset_add(res, metadataInfo);
            }
        }
        close(fd_ficheiro);
    }
    
    strset_free(set_ficheiros);
    
    return res;
}



