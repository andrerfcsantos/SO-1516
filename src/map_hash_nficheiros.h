#ifndef MAP_HASH_NFICHEIROS_H
#define MAP_HASH_NFICHEIROS_H
#include "avl.h"

struct s_map_hash_nficheiros{
    AVL arvore;
};

typedef struct s_map_hash_nficheiros *MAP_HASH_NFICHEIROS;

MAP_HASH_NFICHEIROS mhnf_inicializa(void);
void mhnf_addHash(MAP_HASH_NFICHEIROS m, char *hash);
void mhnf_decrementaHash(MAP_HASH_NFICHEIROS m, char *hash, int deleteIfZero);
void mhnf_eliminaHash(MAP_HASH_NFICHEIROS m, char *hash);
int mhnf_getNFicheirosHash(MAP_HASH_NFICHEIROS m, char *hash);
void mhnf_free(MAP_HASH_NFICHEIROS m);
void mhnf_print(MAP_HASH_NFICHEIROS m);

#endif

