#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>
#include "pedido.h"
#include "resposta.h"
#include "stringset.h"
#include "utils.h"

int initCliente(void);
void trataPedidoFicheiros(TIPO_PEDIDO p_tp, char **ficheiros);
void trataPedidoGC(void);
void trataPedidoBackupDir(char *directoria);

char *nomePipePedidos;
char *nomePipeResposta;
char *nomePipeFicheiros;

int main(int argc, char** argv) {
    int pedidoValido = 0;

        if(initCliente()==0){
        fprintf(stderr, "[CLIENTE - ERRO] Impossivel inicializar cliente. A sair...\n");
        return (EXIT_SUCCESS) ;
    }
    
    switch (argc) {
        case 1:
            printf("Nenhuma acção solicitada.\n");
            break;
        case 2:
            if (strcmp(argv[1], "gc") == 0) {
                trataPedidoGC();
            }else{
                printf("Comando %s nao reconhecido.\n", argv[1]);
            }
            break;
        default:
            if (strcmp(argv[1], "backup") == 0) {
                pedidoValido = 1;
                trataPedidoFicheiros(BACKUP, &argv[2]);
            }

            if (pedidoValido != 1 && strcmp(argv[1], "restore") == 0) {
                pedidoValido = 1;
                trataPedidoFicheiros(RESTORE, &argv[2]);
            }
            
            if (pedidoValido != 1 && strcmp(argv[1], "delete") == 0) {
                pedidoValido = 1;
                trataPedidoFicheiros(DELETE, &argv[2]);
            }
            
            if (pedidoValido != 1 && strcmp(argv[1], "backupdir") == 0) {
                pedidoValido = 1;
                trataPedidoBackupDir(argv[2]);
            }
            
            if (!pedidoValido)  printf("Comando %s nao reconhecido.\n", argv[1]);

            break;

    }
    free(nomePipePedidos);
    free(nomePipeResposta);
    free(nomePipeFicheiros);
    
    return (EXIT_SUCCESS);
}

int initCliente() {
    int res_asprintf;
    int res_mkfifo;
    pid_t pid = getpid();
    
    nomePipePedidos = strdup("Backup/pipe_pedidos");
    //Cria pipe respostas se não existir
    res_asprintf = asprintf(&nomePipeResposta, "Backup/resposta_%d", pid);
    res_mkfifo = mkfifo(nomePipeResposta, 0666);
    
    
    if(res_asprintf == -1 || res_asprintf==-1){
        fprintf(stderr, "[CLIENTE - ERRO] Erro ao criar pipe de resposta. A sair do programa...\n");
        return 0;
    }
    
    res_asprintf = asprintf(&nomePipeFicheiros, "Backup/ficheiros_%d", pid);
    res_mkfifo = mkfifo(nomePipeFicheiros, 0666);
    
    if(res_asprintf == -1 || res_asprintf==-1){
        fprintf(stderr, "[CLIENTE - ERRO] Erro ao criar pipe de ficheiros. A sair do programa...\n");
        return 0;
    }
    
    return 1;
}

void trataPedidoFicheiros(TIPO_PEDIDO p_tp, char **ficheiros) {
    int fd_pipePedidos, fd_pipeResposta, fd_pipeFicheiros;
    int escrito,lido, i, tamanhoEscrita;
    RESPOSTA r;
    AP_PEDIDO p = inicializaPedido(p_tp, getpid());
    
    //Abre pipe pedidos
    fd_pipePedidos = open(nomePipePedidos, O_WRONLY);
    if(fd_pipePedidos==-1){
        fprintf(stderr, "[CLIENTE - ERRO] Impossivel abrir pipe de pedidos. A sair...\n");
        return;
    }
    
    //Envia pedido ao servidor
    escrito = write(fd_pipePedidos, p, sizeof(PEDIDO));
    if(escrito==-1){
        fprintf(stderr, "[CLIENTE - ERRO] Impossivel enviar pedido ao servidor. A sair...\n");
        return;
    } else printf("[CLIENTE - ENVIADO] %s\n",pedidoToString(p));
    
    //Abre pipe de respostas para leitura.
    //Deve ser aberto apenas depois do pedido ter sido enviado
    fd_pipeResposta = open(nomePipeResposta, O_RDONLY);
    if(fd_pipeResposta==-1){
        fprintf(stderr, "[CLIENTE - ERRO] Impossivel abrir pipe para receber respostas. A sair...\n");
        return;
    }
    
    //Le resposta de aceitação do pedido
    lido=read(fd_pipeResposta, &r, sizeof(RESPOSTA));
    if (lido == -1){
        fprintf(stderr, "[CLIENTE - ERRO] Erro ao ler resposta do pipe. A sair...\n");
        return;
    } else printf("[CLIENTE - RECEBIDO] %s\n",respostaToString(&r));
    
    if(r.tr == OK){
        
        fd_pipeFicheiros = open(nomePipeFicheiros, O_WRONLY);
        if(fd_pipeFicheiros==-1){
            fprintf(stderr, "[CLIENTE - ERRO] Impossivel abrir pipe para receber ficheiros. A sair...\n");
            return;
        }
        
        for(i=0;ficheiros[i]; i++){
            tamanhoEscrita = strlen(ficheiros[i]) + 1;
            
            escrito = write(fd_pipeFicheiros, ficheiros[i],tamanhoEscrita);
            if (escrito == -1){
                fprintf(stderr, "[CLIENTE - ERRO] Erro a escrever ficheiro %s no pipe\n",ficheiros[i]);
            }
            else printf("[CLIENTE - ENVIADO] Ficheiro: %s\n", ficheiros[i]);
                
            
            lido = read(fd_pipeResposta, &r, sizeof(RESPOSTA));
            if (lido == -1){
                printf("[CLIENTE - ERRO] Erro ao ler resposta de ficheiro\n");
            } else{
                printf("[CLIENTE - RECEBIDO] %s (Ficheiro: %s)\n",respostaToString(&r), ficheiros[i]);
            }
            
        }
        
        //Close para indicar ao servidor que terminou envio de ficheiros
        close(fd_pipeFicheiros);
        
        
    }
    
    close(fd_pipePedidos);
    close(fd_pipeResposta);
    freePedido(p);
}

void trataPedidoGC() {
    int fd_pipePedidos, fd_pipeResposta;
    int escrito,lido;
    RESPOSTA r;
    AP_PEDIDO p = inicializaPedido(GC, getpid());
    
    //Abre pipe pedidos
    fd_pipePedidos = open(nomePipePedidos, O_WRONLY);
    if(fd_pipePedidos==-1){
        fprintf(stderr, "[CLIENTE - ERRO] Impossivel abrir pipe de pedidos. A sair...\n");
        return;
    }
    
    //Envia pedido ao servidor
    escrito = write(fd_pipePedidos, p, sizeof(PEDIDO));
    if(escrito==-1){
        fprintf(stderr, "[CLIENTE - ERRO] Impossivel enviar pedido ao servidor. A sair...\n");
        return;
    } else printf("[CLIENTE - ENVIADO] %s\n",pedidoToString(p));
    
    //Abre pipe de respostas para leitura.
    //Deve ser aberto apenas depois do pedido ter sido enviado
    fd_pipeResposta = open(nomePipeResposta, O_RDONLY);
    if(fd_pipeResposta==-1){
        fprintf(stderr, "[CLIENTE - ERRO] Impossivel abrir pipe para receber respostas. A sair...\n");
        return;
    }
    
    //Le resposta de aceitação do pedido
    lido=read(fd_pipeResposta, &r, sizeof(RESPOSTA));
    if (lido == -1){
        fprintf(stderr, "[CLIENTE - ERRO] Erro ao ler resposta do pipe. A sair...\n");
        return;
    } else printf("[CLIENTE - RECEBIDO] %s\n",respostaToString(&r));

    if (r.tr == OK) {
        lido = read(fd_pipeResposta, &r, sizeof (RESPOSTA));
        
        if (lido == -1){
            fprintf(stderr, "[CLIENTE - ERRO] Impossivel ler resposta.\n");
        } else printf("[CLIENTE - RECEBIDO] %s\n", respostaToString(&r));

    }
    
    close(fd_pipePedidos);
    close(fd_pipeResposta);
    freePedido(p);
}

void trataPedidoBackupDir(char *directoria) {
    char **ficheiros;
    STRINGSET set_ficheiros;
    
    set_ficheiros = daFicheirosDir(directoria);
    ficheiros = strset_toArray(set_ficheiros);
    
    trataPedidoFicheiros(BACKUP_DIR, ficheiros);
    
    strset_freeArray(ficheiros);
    strset_free(set_ficheiros);
}



