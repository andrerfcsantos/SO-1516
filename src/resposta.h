#ifndef RESPOSTA_H
#define RESPOSTA_H

enum e_tipo_resposta{
    OK,
    NOT_OK,
    NO_ACCESS_TO_FILE,
    BACKUP_ERROR,
    RESTORE_ERROR,
    DELETE_ERROR,
    GC_ERROR,
    SERVER_BUSY
};
typedef enum e_tipo_resposta TIPO_RESPOSTA;


struct s_resposta{
    TIPO_RESPOSTA tr;
};
typedef struct s_resposta *AP_RESPOSTA;
typedef struct s_resposta RESPOSTA;

AP_RESPOSTA inicializaResposta(TIPO_RESPOSTA p_tr);
char *respostaToString(AP_RESPOSTA r);
void freeResposta(AP_RESPOSTA r);
char *tipoRespostaToString(TIPO_RESPOSTA r);


#endif