#ifndef PEDIDO_H
#define PEDIDO_H

#include <sys/types.h>
#include <sys/wait.h>

enum e_tipo_pedido {
    BACKUP,
    RESTORE,
    DELETE,
    GC,
    BACKUP_DIR
};

typedef enum e_tipo_pedido TIPO_PEDIDO;

struct s_pedido{
    TIPO_PEDIDO tp;
    pid_t pid;
};

typedef struct s_pedido *AP_PEDIDO;
typedef struct s_pedido PEDIDO;


AP_PEDIDO inicializaPedidoVazio();
AP_PEDIDO inicializaPedido(TIPO_PEDIDO p_tp, pid_t p_pid);
char *pedidoToString(AP_PEDIDO p);
void freePedido(AP_PEDIDO p);
char *tipoPedidoToString(TIPO_PEDIDO tp);

#endif

