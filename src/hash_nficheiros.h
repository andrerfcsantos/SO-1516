#ifndef HASH_NFICHEIROS_H
#define HASH_NFICHEIROS_H

struct s_hash_nficheiros{
    char *hash;
    int nFicheiros;
};

typedef struct s_hash_nficheiros *HASH_NFICHEIROS;

HASH_NFICHEIROS hnf_inicializaVazio(void);
HASH_NFICHEIROS hnf_inicializaComHash(char *hash);
HASH_NFICHEIROS hnf_inicializa(char *hash, int n);
int hnf_getNFicheiros(HASH_NFICHEIROS p);
char *hnf_getHash(HASH_NFICHEIROS p);
char *hnf_toString(HASH_NFICHEIROS p);
void hnf_free(HASH_NFICHEIROS p);

#endif

