#ifndef UTILS_H
#define UTILS_H

#include <unistd.h>
#include "stringset.h"
#include "map_hash_nficheiros.h"

#define PIPE_ESCRITA 1
#define PIPE_LEITURA 0
#define MAX_SIZE_PATH 32768
#define MAX_LINHA 32768

ssize_t leLinha(int fildes, char *buff_original, size_t nbyte);
STRINGSET daFicheirosDir(char *directoria);
MAP_HASH_NFICHEIROS loadMetadata(char *metadataPath);
STRINGSET loadHashsMetadata(char *metadataPath);


#endif

