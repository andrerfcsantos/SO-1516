#ifndef STRINGSET_H
#define STRINGSET_H
#include "avl.h"

struct s_stringset{
    AVL arvore;
};

typedef struct s_stringset *STRINGSET;

STRINGSET strset_inicializa(void);
void strset_add(STRINGSET set, char *str);
void strset_remove(STRINGSET set, char *str);
int strset_existestr(STRINGSET set, char *str);
char **strset_toArray(STRINGSET set);
void strset_freeArray(char **a);
void strset_free(STRINGSET s);
void strset_print(STRINGSET s);

#endif

