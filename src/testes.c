#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>

#include "map_hash_nficheiros.h"
#include "stringset.h"

void testeFilhos(void);
void sigchldHandler(int signum);
void printHome(void);
void iteraDirectoria(void);
void iteraMetadata(void);
void iteraData(void);
void testeLeLinha(void);
ssize_t leLinha(int fildes, char *buff_original, size_t nbyte);
ssize_t leLinha2(int fildes, char *buff_original, size_t nbyte);
void testeCicloFicheirosDir(void);
STRINGSET daFicheirosDir(char *directoria);

int numberOfChilds=0;

int main(int argc, char** argv) {
    pid_t filho;
    int estado;
    filho = fork();
    
    if(filho==0){
        execlp("pwd", "pwd", NULL);
        _exit(112);
    }else{
        waitpid(filho, &estado, 0);
    }
    
    return (EXIT_SUCCESS);
}

void testeFilhos() {
    signal(SIGCHLD, sigchldHandler);
    pid_t pidChild = fork();

    if (pidChild == 0) {
        fork();
        sleep(2);
    } else {
        numberOfChilds++;
        sleep(4);
    }
}

void sigchldHandler(int signum){
    if(signum == SIGCHLD){
        numberOfChilds--;
        printf("Current number of processes: %d\n", numberOfChilds);
    }
}

void printHome(){
    printf("home = %s\n", getenv("HOME"));
}

void iteraDirectoria() {
    DIR *dir;
    struct dirent *ent;
    
    if ((dir = opendir("./")) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (ent->d_type == 8) printf("%s | %d\n", ent->d_name, ent->d_type);
        }
        closedir(dir);
    } else {
        perror("");
    }
}

void iteraMetadata() {
    char *metadataPath = "Backup/metadata/";
    char *pathFicheiroCompleto;
    int fd_ficheiro;
    MAP_HASH_NFICHEIROS map = mhnf_inicializa();
    DIR *dir;
    char metadataInfo[41];
    struct dirent *ent;
    
    if ((dir = opendir(metadataPath)) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (ent->d_type == 8){
                asprintf(&pathFicheiroCompleto, "%s%s", metadataPath, ent->d_name);
                fd_ficheiro = open(pathFicheiroCompleto, O_RDONLY);
                
                if(fd_ficheiro!=-1){
                    read(fd_ficheiro, metadataInfo, 40);
                    metadataInfo[40] = '\0';
                    mhnf_addHash(map,metadataInfo);
                }else{
                    printf("Erro ao abrir ficheiro.\n");
                }
                
                free(pathFicheiroCompleto);
                close(fd_ficheiro);
            }
        }
        closedir(dir);
    } else {
        perror("");
    }
    
    mhnf_print(map);
    mhnf_free(map);
}

void iteraData(){
    char *metadataPath = "Backup/data/";
    STRINGSET set = strset_inicializa();
    DIR *dir;
    struct dirent *ent;
    
    if ((dir = opendir(metadataPath)) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (ent->d_type == 8){
                strset_add(set, ent->d_name);
            }
        }
        closedir(dir);
    } else {
        perror("");
    }
    
    strset_print(set);
    strset_free(set);
}

void testeLeLinha(){
    char linha[2048];
    int lido, i=0;
    int fd = open("notas_delete_gc.txt", O_RDONLY);
    
    while((lido = leLinha(fd, linha, 2048))!=0){
        printf("linha %d: %s\n", i, linha);
        i++;
    }
    
    close(fd);
}

ssize_t leLinha(int fildes, char *buff_original, size_t nbyte){
    int end =0;
    ssize_t lido;
    size_t nbytes_leitura = 1;
    size_t nbytes_lidos_total = 0;
    
    while(nbytes_lidos_total < nbyte && !end) {
        end=0;
        
        lido = read(fildes,buff_original+nbytes_lidos_total,nbytes_leitura);
        
        if(buff_original[nbytes_lidos_total] == '\n' || lido <=0){
            end=1;
            buff_original[nbytes_lidos_total]='\0';
        }
        nbytes_lidos_total += lido;
        
    }
    
    return nbytes_lidos_total;
}

ssize_t leLinha2(int fildes, char *buff_original, size_t nbyte){
    ssize_t lido;
    size_t nbytes_leitura = 1;
    size_t nbytes_lidos_total = 0;
    
    while(nbytes_lidos_total < nbyte && 
         (lido = read(fildes,buff_original+nbytes_lidos_total,nbytes_leitura)) > 0 &&
         buff_original[nbytes_lidos_total] != '\n') {
        
        nbytes_lidos_total += lido;
        
    }
    
    return nbytes_lidos_total;
}

void testeCicloFicheirosDir(){
    int i=0;
    STRINGSET ficheiros = daFicheirosDir("Backup/metadata/");
    
    char **array_ficheiros = strset_toArray(ficheiros);
    
    while(array_ficheiros[i]){
        printf("Ficheiro %d : %s\n",i, array_ficheiros[i]);
        i++;
    }
    
    strset_free(ficheiros);
}

STRINGSET daFicheirosDir(char *directoria){
    int estado, lido;
    char fichLido[1024];
    int fd_pipe[2];
    pid_t pid_filho;
    STRINGSET res = strset_inicializa();
    
    pipe(fd_pipe);
    pid_filho = fork();
    
    if(pid_filho==0){
        dup2(fd_pipe[1],1);
        close(fd_pipe[1]);
        close(fd_pipe[0]);
        execlp("find", "find", directoria, "-type", "f", NULL);
        _exit(112);
    }else{
        close(fd_pipe[1]);
        while((lido = leLinha(fd_pipe[0], fichLido ,1024))!=0){
            strset_add(res, fichLido);
        }
        close(fd_pipe[0]);
        waitpid(pid_filho, &estado,0);
    }
    
    return res;
}